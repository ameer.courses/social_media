// ignore_for_file: prefer_const_constructors, unnecessary_new, prefer_const_literals_to_create_immutables


import 'package:flutter/material.dart';
import 'package:social_media/Logic/Controllers/AuthController.dart';
import 'package:social_media/UI/Screens/Auth/login.dart';
import 'UI/Screens/Auth/register.dart';
import 'UI/Screens/User/Profile.dart';
import 'UI/Screens/User/home.dart';

import 'package:get_storage/get_storage.dart';

void main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light().copyWith(
        appBarTheme: AppBarTheme(
          centerTitle: true,
          color: Colors.indigo,
          //foregroundColor: Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(AppBar().preferredSize.height*0.35),
            )
          )
        ),

      ),

      routes: {
        '/login' : (c) => LoginScreen(),
        '/register' : (c) => RegisterScreen(),
        '/home' : (c) => HomeScreen(),
        '/profile' : (c) => Profile()
      },

      initialRoute: GetStorage().hasData('token') ? '/home' : '/login',

    );
  }
}







