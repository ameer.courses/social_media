// ignore_for_file: curly_braces_in_flow_control_structures, prefer_const_constructors

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_media/Logic/Controllers/postController.dart';
import 'package:social_media/Logic/STM/HomeController.dart';


class Profile extends StatefulWidget {
  String? _imgPath;
  final TextEditingController desController = TextEditingController();
  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Column(
        children: [
          GestureDetector(
            onTap: () async {
              XFile? xfile = await ImagePicker().pickImage(source: ImageSource.gallery);
              if(xfile != null)
                setState(() {
                  widget._imgPath = xfile.path;
                });
            },
            child: Container(
              height: size.height*0.2,
              width: double.infinity,
              margin: EdgeInsets.all(size.width*0.025),
              padding: EdgeInsets.all(size.width*0.025),
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(size.width*0.025),
                  image: widget._imgPath != null ? DecorationImage(
                      image: FileImage(File(widget._imgPath!)),
                      fit: BoxFit.cover
                  ) : null
              ),
              child: Icon(Icons.add_a_photo_outlined),
              alignment: Alignment.center,
            ),
          ),
          TextField(
            controller: widget.desController,
            decoration: InputDecoration(
              hintText: 'Add description'
            ),
          ),
          ElevatedButton.icon(
              onPressed: () async{
                if(widget._imgPath != null || widget.desController.text.isNotEmpty)
                {
                  var res = await PostController.addPost(widget.desController.text, widget._imgPath!);
                  if(res == null)
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error!!')));
                  else{
                    HomeController c = Get.find();
                    c.reloadHome();
                    Navigator.pop(context);
                  }
                }
                else
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Missing Data')));
              },
              icon: Icon(Icons.post_add),
              label: Text('Post')
          )
        ],
      ),
    );
  }
}
