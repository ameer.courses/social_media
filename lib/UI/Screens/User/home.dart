// ignore_for_file: prefer_const_constructors, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:social_media/Logic/Controllers/AuthController.dart';
import 'package:social_media/Logic/Controllers/postController.dart';
import 'package:social_media/Logic/Models/postUserModel.dart';
import 'package:social_media/Logic/STM/HomeController.dart';
import 'package:social_media/UI/Elements/post.dart';

class HomeScreen extends StatelessWidget {

  static final HomeController _controller = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: GetBuilder<HomeController>(
        init: _controller,
        builder: (controller) {
          return FutureBuilder< List<PostUserModel> >(
            future: PostController.getHome(),
            builder: (context,snapShot){ //AsyncSnapShot
              if(snapShot.hasData)
                return ListView.builder(
                    itemCount: snapShot.data!.length,
                    itemBuilder: (context,i) => PostWidget(snapShot.data![i])
                );

              if(snapShot.hasError)
                return Center(
                  child: Text(snapShot.error.toString()),
                );

              return Center(
                child: CircularProgressIndicator(),
              );
            },
          );
        }
      ),
      drawer: Drawer(
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            DrawerHeader(
                padding: EdgeInsets.zero,
                child: Container(
                  color: Colors.indigo,
                ),
            ),
            ListTile(
              title: Text('Profile'),
              leading: Icon(Icons.person_outline),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: (){
                Navigator.pushNamed(context, '/profile');
              },
            ),
            ListTile(
              title: Text('Settings'),
              leading: Icon(Icons.settings_outlined),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: (){},
            ),
            ListTile(
              title: Text('Logout'),
              leading: Icon(Icons.logout_outlined),
              onTap: () async {
                String message = await AuthController.logout();
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(message)));
                Navigator.pushReplacementNamed(context, '/login');
              },
            ),
            Divider(
              color: Colors.black,
            ),
            ListTile(
              title: Text('About'),
              leading: Icon(Icons.info_outlined),
              onTap: (){},
            ),
            ListTile(
              title: Text('Contact us'),
              leading: Icon(Icons.phone),
              onTap: (){},
            ),
            Divider(
              color: Colors.black,
            ),
            ListTile(
              title: Text('Report Errors'),
              leading: Icon(Icons.report_gmailerrorred_outlined),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: (){},
            ),


          ],
        ),
      ),
    );
  }
}
