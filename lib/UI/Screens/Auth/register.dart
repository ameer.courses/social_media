// ignore_for_file: prefer_const_constructors, curly_braces_in_flow_control_structures

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_media/Logic/Controllers/AuthController.dart';
import 'package:social_media/Logic/Models/userModel.dart';
import 'package:social_media/UI/Elements/myTextFields.dart';

class RegisterScreen extends StatefulWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();

  String? _imgPath;

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: size.height*0.45,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(size.width*0.1)),
              ),
              child: SafeArea(
                child: Column(
                  children: [
                    Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(size.width*0.05),
                          child: FittedBox(
                              child: Text('Register',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold
                                ),
                              )
                          ),
                        )
                    ),
                    Expanded(
                      flex: 2,
                      child: Image.asset('images/c1.png'),
                    ),

                  ],
                ),
              ),
            ),

            AuthField(
              controller: widget.nameController,
              hint:'name' ,
            ),
            AuthField(
              controller: widget.emailController,
              hint:'email' ,
              keyboardType: TextInputType.emailAddress,
            ),
            AuthField(
              controller: widget.passController,
              hint: 'password',
              isPass: true,
            ),
            AuthField(
              controller: widget.phoneController,
              hint: 'phone',
              keyboardType: TextInputType.phone,
            ),
            GestureDetector(
              onTap: () async {
                XFile? xfile = await ImagePicker().pickImage(source: ImageSource.gallery);
                if(xfile != null)
                  setState(() {
                    widget._imgPath = xfile.path;
                  });
              },
              child: Container(
                height: size.height*0.2,
                width: double.infinity,
                margin: EdgeInsets.all(size.width*0.025),
                padding: EdgeInsets.all(size.width*0.025),
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(size.width*0.025),
                  image: widget._imgPath != null ? DecorationImage(
                    image: FileImage(File(widget._imgPath!)),
                    fit: BoxFit.cover
                  ) : null
                ),
                child: Icon(Icons.add_a_photo_outlined),
                alignment: Alignment.center,
              ),
            ),


            ElevatedButton.icon(
              onPressed: ()async{
                var res = await AuthController.register(RegisterModel(
                    name: widget.nameController.text,
                    email: widget.emailController.text,
                    password: widget.passController.text,
                    phone: widget.phoneController.text,
                    image: widget._imgPath
                )
                );
                if(res != null)
                Navigator.pushReplacementNamed(context, '/home');
                else
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Register failed!'),));
              },
              label: Text('Register'),
              icon: Icon(Icons.login),
            ),
            Padding(
                padding: EdgeInsets.all(size.width*0.025),
                child: InkWell(
                    child: Text('if you have an account login.'),
                  onTap: (){
                    Navigator.pushReplacementNamed(context, '/login');
                  },
                ),
            )
          ],
        ),
      ),
    );
  }
}
