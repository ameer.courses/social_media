// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:social_media/Logic/Controllers/AuthController.dart';
import 'package:social_media/Logic/Models/userModel.dart';
import 'package:social_media/UI/Elements/myTextFields.dart';
import 'package:social_media/UI/Screens/Auth/register.dart';

class LoginScreen extends StatelessWidget {


  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: size.height*0.45,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.vertical(bottom: Radius.circular(size.width*0.1)),
                ),
                child: SafeArea(
                  child: Column(
                    children: [
                      Expanded(
                          child: Padding(
                            padding: EdgeInsets.all(size.width*0.05),
                            child: FittedBox(
                                child: Text('Login',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                                ),
                                )
                            ),
                          )
                      ),
                      Expanded(
                          flex: 2,
                          child: Image.asset('images/c1.png'),
                      ),

                    ],
                  ),
                ),
              ),

              AuthField(
                  controller: emailController,
                  hint:'email' ,
                  keyboardType: TextInputType.emailAddress,
              ),
              AuthField(
                  controller: passController,
                hint: 'password',
                isPass: true,
              ),

              ElevatedButton.icon(
                  onPressed: () async {
                    UserModel? model = await AuthController.login(emailController.text, passController.text);
                    if(model != null)
                    Navigator.pushReplacementNamed(context, '/home');
                    else
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error')));
                  },
                  label: Text('Login'),
                  icon: Icon(Icons.login),
              ),
              Padding(
                padding: EdgeInsets.all(size.width*0.025),
                child: InkWell(
                    child: Text('if you don\'t have an account register.'),
                    onTap: (){
                        Navigator.pushReplacementNamed(context, '/register');
                    },
                ),
              )
            ],
          ),
        ),
    );
  }
}
