// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:social_media/Logic/Controllers/postController.dart';
import 'package:social_media/Logic/Models/postModel.dart';
import 'package:social_media/Logic/Models/postUserModel.dart';
import 'package:social_media/Logic/Models/userModel.dart';
import 'package:social_media/Logic/STM/HomeController.dart';


class PostWidget extends StatelessWidget{
  
  final PostUserModel model;



  PostWidget(this.model);

  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: size.height * (0.4 + model.post.description.split('\n').length * 0.02 ) ,
      margin: EdgeInsets.all(size.width*0.025),
      decoration: BoxDecoration(
        color: Colors.deepPurple.withOpacity(0.2),
        borderRadius: BorderRadius.circular(size.width*0.025)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            leading: model.user.image != null ? CircleAvatar(
              foregroundImage: NetworkImage(model.user.image!) ,
            ): Icon(Icons.person_outline) ,
            title: Text(model.user.name),
            subtitle: Text(model.post.created_at.substring(0,9)),
          ),
          Divider(),
          Padding(
              padding: EdgeInsets.all(size.width*0.025),
              child: Text(model.post.description),
          ),
          Expanded(
              child: GestureDetector(
                onTap: (){
                  showDialog(
                      context: context,
                      builder: (context) =>
                          Dialog(
                            insetPadding: EdgeInsets.zero,
                            child: SizedBox(
                              width: double.infinity,

                              child: InteractiveViewer(
                                child: Image.network(model.post.image,fit: BoxFit.cover,),
                              ),
                            ),
                          )
                  );
                },
                child: SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: Image.network(
                    model.post.image,
                   fit: BoxFit.cover,
          ),
                ),
              )
          ),
          Reactions(model.post)
        ],
      ),
    );
  }


}

class Reactions extends StatelessWidget {

  final PostModel post;


  Reactions(this.post);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height*0.075,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            children: [
              Text(post.likes.toString()),
              SizedBox(width: size.width*0.01,),
              IconButton(
                  onPressed: ()async{
                    await PostController.react(true, post.id);
                    HomeController c = Get.find();
                    c.reloadHome();
                  },
                  icon: Icon(Icons.thumb_up_outlined),
              ),
            ],
          ),
          Row(
            children: [
              Text(post.dislikes.toString()),
              SizedBox(width: size.width*0.01,),
              IconButton(
                onPressed: ()async{
                  await PostController.react(false, post.id);
                  HomeController c = Get.find();
                  c.reloadHome();
                },
                icon: Icon(Icons.thumb_down_outlined),
              ),
            ],
          ),
          Row(
            children: [
              Text('0'),
              SizedBox(width: size.width*0.01,),
              Icon(Icons.comment_outlined),
            ],
          ),
        ],
      ),
    );
  }
}
