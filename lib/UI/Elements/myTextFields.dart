import 'package:flutter/material.dart';


class AuthField extends StatefulWidget {

  final TextEditingController controller;
  final String? hint;
  final Widget? prefixIcon;
  final bool isPass;
  final TextInputType? keyboardType;
  bool _hide = true;

  AuthField({
    required this.controller,
    this.keyboardType,
    this.hint,
    this.prefixIcon,
    this.isPass = false
  });

  @override
  State<AuthField> createState() => _AuthFieldState();
}

class _AuthFieldState extends State<AuthField> {


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      child: TextField(
        controller: widget.controller,
        decoration: InputDecoration(
            hintText: widget.hint,
            prefixIcon: widget.prefixIcon,
            suffixIcon: (widget.isPass)? IconButton(
                onPressed: (){
                  setState(() {
                    widget._hide = ! widget._hide;
                  });
                },
                icon: widget._hide ? Icon(Icons.visibility_outlined) : Icon(Icons.visibility_off_outlined)
            ):null,
            border: InputBorder.none
        ),
        obscureText: widget._hide && widget.isPass,
        keyboardType: widget.keyboardType,
      ),
      margin: EdgeInsets.all(size.width*0.025),
      padding: EdgeInsets.all(size.width*0.025),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.2),
        borderRadius: BorderRadius.circular(size.width*0.025),

      ),
    );
  }
}
