
import 'dart:convert';

class PostModel{

  final String description;
  final int id;
  final String image;
  final String created_at;
  final int likes;
  final int dislikes;

  PostModel({
    required this.description,
    required this.id,
    required this.image,
    required this.created_at,
    required this.dislikes,
    required this.likes
  });

  factory PostModel.fromJSON(Map<String,dynamic> json) =>
      PostModel(
          description: json['description'],
          id: json['id'],
          image: json['image'],
          created_at: json['created_at'],
          dislikes: json['dislikes'] ?? 0,
          likes: json['likes'] ?? 0,
      );

}