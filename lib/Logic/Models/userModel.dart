
class UserModel {
  final int id;
  final String name;
  final String email;
  final String phone;
  final String? image;

  UserModel(
      {
        required this.id,
      required this.name,
      required this.email,
      required this.phone,
       this.image
      });


  factory UserModel.fromJSON(Map<String,dynamic> json) =>
      UserModel(
          id: json['id'],
          name: json['name'],
          email: json['email'],
          phone: json['phone'],
          image: json['image']
      );


}

class RegisterModel {

  final String name;
  final String email;
  final String password;
  final String phone;
  final String? image;



  RegisterModel({
    required this.name,
    required this.email,
    required this.password,
    required this.phone,
    this.image,
  });


  Map<String, String> get toMap => {
    "name": name,
    "email": email,
    "password": password,
    "phone": phone,
  };
}
