
import 'package:social_media/Logic/Models/postModel.dart';
import 'package:social_media/Logic/Models/userModel.dart';

class PostUserModel{

  final PostModel post;
  final UserModel user;

  PostUserModel(this.post,this.user);
}