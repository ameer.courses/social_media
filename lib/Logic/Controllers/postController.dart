// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

import 'package:social_media/Logic/Models/postModel.dart';
import 'package:social_media/Logic/Models/postUserModel.dart';
import 'package:social_media/Logic/Models/userModel.dart';

class PostController{

  static Future<List<PostUserModel>> getHome() async {
    var response = await http.get(Uri.parse('http://10.0.2.2:8000/api/home'));
    Map<String,dynamic> json = jsonDecode(response.body);

    List posts = json['posts'];
    List<PostUserModel> models = [];

    for(var pu in posts)
      models.add(PostUserModel(PostModel.fromJSON(pu['post']), UserModel.fromJSON(pu['user'])));

    return models;
  }

  static Future<PostModel?> addPost(String description, String image) async {
    var request = http.MultipartRequest('POST', Uri.parse('http://10.0.2.2:8000/api/post/add'));

    request.fields.addAll({
      'description' : description
    });

    request.headers.addAll({
      'Accept': 'application/json',
      'Authorization': 'Bearer ${GetStorage().read('token')}'
    });


      request.files.add(await http.MultipartFile.fromPath('image', image));
    var response = await request.send();

    Map<String,dynamic> json = jsonDecode(await response.stream.bytesToString());

    if(json['post'] == null)
      return null;

    return PostModel.fromJSON(json['post']);
  }

  static Future<void> react(bool value,int id) async {
    var response = await http.post(
        Uri.parse('http://10.0.2.2:8000/api/post/reaction/$id'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}',
        },
      body: {
          'value': value ? 'like' : 'dislike'
      }
    );

    print(response);
  }

}