import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:social_media/Logic/Models/userModel.dart';
import 'package:get/get.dart';


class AuthController{


  static Future<UserModel?> login(String email,String password) async {
    var response = await http.post(
        Uri.parse('http://10.0.2.2:8000/api/login'), //http://192.168.137.1:8000/api/login
      body: {
        "email":email,
        "password":password
      }
    );
    Map<String,dynamic> json = jsonDecode(response.body);
    if(json["error"] != null){
      return null;
    }
    await GetStorage().write('token', json['token']);
    return UserModel.fromJSON(json['user']);
  }
  
  static Future<String> logout() async {
    var response = await http.get(
        Uri.parse('http://10.0.2.2:8000/api/logout'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}'
        }
    );
    Map<String,dynamic> json = jsonDecode(response.body);

    await GetStorage().remove('token');

    return json['message'];
  }

  static Future<UserModel?> register(RegisterModel model) async {
    var request = http.MultipartRequest('POST', Uri.parse('http://10.0.2.2:8000/api/register'));

    request.fields.addAll(model.toMap);

    if(model.image != null)
    request.files.add(await http.MultipartFile.fromPath('image', model.image!));

    var response = await request.send();

    Map<String,dynamic> json = jsonDecode(await response.stream.bytesToString());

    if(json['token'] == null)
      return null;

    await GetStorage().write('token', json['token']);
    return UserModel.fromJSON(json['user']);
  }


}
